const toDoBusiness = require('../../business/toDo.business');
const taskMongooseModel = require('../../models/task.model');

exports.createTask = async (req, res) => {
    try {
        const task = new taskMongooseModel(req.body);
        await toDoBusiness.createTask(task);
        res.json({ task });
    } catch (error) {
        console.log(error);
        res.status(500).send('Something is wrong');
    }
};
exports.getAllTasks = async (req, res) => {
    try {
        var tasks = await toDoBusiness.getAllTasks();
        res.json({ tasks });
    } catch (error) {
        console.log(error);
        res.status(500).send('Something is wrong');
    }
};
exports.updateTask = async (req, res) => {
    try {
        const { name, state } = req.body;

        // Check if the task exists
        let task = await toDoBusiness.getTaskById(req.params.id);
        if (!task) res.status(400).json({ msg: 'Task not found' });

        const newTask = {};
        newTask.name = name;
        newTask.state = state;

        // update the task
        task = await toDoBusiness.updateTask(req.params.id, newTask);
        res.json({ task });
    } catch (error) {
        console.log(error);
    }
};
exports.deleteTask = async (req, res) => {
    try {
        // Check if the task exists
        let task = await toDoBusiness.getTaskById(req.params.id);
        if (!task) res.status(400).json({ msg: 'Task not found' });

        // delete the task
        await toDoBusiness.deleteTask(req.params.id);
        res.json({ msg: 'Task deleted' });
    } catch (error) {
        console.log(error);
    }
};