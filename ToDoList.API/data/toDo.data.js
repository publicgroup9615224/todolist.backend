var mongoose = require('mongoose');

const taskMongooseModel = require('../models/task.model');

var toDoDal = {};

toDoDal.createTask = createTask;
toDoDal.getAllTasks = getAllTasks;
toDoDal.getTaskById = getTaskById;
toDoDal.updateTask = updateTask;
toDoDal.deleteTask = deleteTask;

module.exports = toDoDal;

async function createTask(taskModel) {
    return await taskModel.save();
}

async function getAllTasks() {
    let tasks = await taskMongooseModel.find({});
    return tasks;
}

async function getTaskById(taskId) {
    let task = await taskMongooseModel.findById(taskId);
    return task;
}

async function updateTask(taskId, taskModel) {
    let task = await taskMongooseModel.findByIdAndUpdate({ _id: taskId }, taskModel, { new: true });
    return task;
}

async function deleteTask(taskId) {
    await taskMongooseModel.findByIdAndDelete({ _id: taskId });
    return true;
}