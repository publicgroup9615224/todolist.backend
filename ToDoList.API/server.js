const express = require('express');
const conectDB = require('./config/db');
const cors = require('cors');
// create the server
const app = express();

const PORT = process.env.PORT || 1337;

// conect to the database
conectDB();

// enable cors
app.use(cors());

// enable express.json
app.use(express.json({ extended: true }));

app.use('/api/tasks', require('./routes/tasks.routes'));

app.listen(PORT, () => {
    console.log('server runing on', PORT);
});
