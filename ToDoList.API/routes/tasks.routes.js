const express = require('express');
const router = express.Router();
const taskController = require('../controllers/api/toDo.api.controller');
const { check } = require('express-validator');

router.post('/', [check('name', 'The name is required').not().isEmpty()], taskController.createTask);
router.post('/tasks', [check('name', 'The name is required').not().isEmpty()], taskController.createTask);
router.get('/', taskController.getAllTasks);
router.get('/tasks', taskController.getAllTasks);
router.put('/:id', taskController.updateTask);
router.delete('/:id', taskController.deleteTask);

module.exports = router;