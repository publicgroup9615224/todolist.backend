const httpMocks = require('node-mocks-http');
const toDoControllerApi = require('../controllers/api/toDo.api.controller');
const taskMongooseModel = require('../models/task.model');
const toDoData = require('../data/toDo.data');
var ObjectId = require('bson').ObjectId;

describe('test toDoControllerApi', () => {

    afterAll(() => {
        jest.resetAllMocks();
    });

    test('test createTask', async () => {

        var jsonRequest = { name: 'Task1', state: true };
        var jsonResult = [{ id: 1, name: 'Task1', state: true, creationDate: new Date() }];

        jest.spyOn(toDoData, 'createTask').mockReturnValue(
            new Promise((resolve, _reject) => resolve(jsonResult))
        )

        const req = httpMocks.createRequest({
            //method: 'POST',
            //url: '/signup',
            body: jsonRequest
          })
        const res = httpMocks.createResponse();

        await toDoControllerApi.createTask(req, res);
        var task = res._getJSONData();

        console.log(task);
        expect(task).not.toBeNull();
        expect(task.creationDate).not.toBeNull();
        expect(task.id).not.toBeNull();
    }); 
    
    test('test updateTask', async () => {

        var objId = new ObjectId();
        var jsonParams = { id: objId.toString() };
        var jsonBody = { name: 'Task2', state: true };
        var jsonUpdateResult = { id: objId.toString(), name: 'Task2', state: true };
        var jsonGetTaskByIdResult = { id: objId.toString(), name: 'Task1', state: true, creationDate: new Date() };

        jest.spyOn(toDoData, 'updateTask').mockReturnValue(
            new Promise((resolve, _reject) => resolve(jsonUpdateResult))
        );
        jest.spyOn(toDoData, 'getTaskById').mockReturnValue(
            new Promise((resolve, _reject) => resolve(jsonGetTaskByIdResult))
        );

        const req = httpMocks.createRequest({
            //method: 'POST',
            //url: '/signup',
            params: jsonParams,
            body: jsonBody
          })
        const res = httpMocks.createResponse();

        await toDoControllerApi.updateTask(req, res);
        var task = res._getJSONData();

        console.log(task);
        expect(task).not.toBeNull();
        expect(task.id).not.toBeNull();
    }); 

    test('test getAllTasks', async () => {

        var jsonResult = [{ id: 1, name: 'Task1', state: true, creationDate: new Date() }];
        jest.spyOn(toDoData, 'getAllTasks').mockReturnValue(
            new Promise((resolve, _reject) => resolve(jsonResult))
        )

        const req = { params: {} };
        const res = httpMocks.createResponse();

        await toDoControllerApi.getAllTasks(req, res);
        var tasks = res._getJSONData();

        console.log(tasks);
        expect(tasks).not.toBeNull();
    });

    test('test deleteTask', async () => {

        var objId = new ObjectId();
        var jsonParams = { id: objId.toString() };
        var jsonDeleteResult = true;
        var jsonGetTaskByIdResult = { id: objId.toString(), name: 'Task1', state: true, creationDate: new Date() };

        jest.spyOn(toDoData, 'deleteTask').mockReturnValue(
            new Promise((resolve, _reject) => resolve(jsonDeleteResult))
        );
        jest.spyOn(toDoData, 'getTaskById').mockReturnValue(
            new Promise((resolve, _reject) => resolve(jsonGetTaskByIdResult))
        );

        const req = httpMocks.createRequest({
            //method: 'POST',
            //url: '/signup',
            params: jsonParams
          })
        const res = httpMocks.createResponse();

        await toDoControllerApi.deleteTask(req, res);
        var apiResult = res._getJSONData();

        console.log(apiResult);
        expect(apiResult).toEqual({ msg: 'Task deleted' });
    }); 
});
