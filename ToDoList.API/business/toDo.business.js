const toDoData = require('../data/toDo.data');

var toDoBusiness = {};

toDoBusiness.createTask = createTask;
toDoBusiness.getAllTasks = getAllTasks;
toDoBusiness.getTaskById = getTaskById;
toDoBusiness.updateTask = updateTask;
toDoBusiness.deleteTask = deleteTask;

module.exports = toDoBusiness;

async function createTask(taskModel) {
    return await toDoData.createTask(taskModel);
}

async function getAllTasks() {
    return await toDoData.getAllTasks();
}

async function getTaskById(taskId) {
    return await toDoData.getTaskById(taskId);
}

async function updateTask(taskId, taskModel) {
    return await toDoData.updateTask(taskId, taskModel);
}

async function deleteTask(taskId) {
    return await toDoData.deleteTask(taskId);
}